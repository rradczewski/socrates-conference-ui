import React from 'react'
import './App.scss'
import {Amplify} from 'aws-amplify'
import config from '../aws-exports'
import {LoginScreen} from '../login/LoginScreen'
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
  useLocation
} from 'react-router-dom'
import {ConferenceProvider} from './ConferenceProvider'
import {
  AuthProvider,
  useAuth
} from './AuthProvider'
import {Conferences} from './api/conferences'
import {ConfirmationScreen} from '../confirm/ConfirmationScreen'
import {
  AttendeeProvider
} from './AttendeeProvider'
import {Attendees} from './api/attendees'
import {ProfileScreen} from '../profile/ProfileScreen'
import {ProfileProvider} from './ProfileProvider'
import {Profile} from './api/profile'
import {BookingProvider} from './BookingProvider'
import {BookingImpl} from './api/booking'

Amplify.configure({
  Auth: {
    region: config.aws_cognito_region,
    userPoolId: config.aws_user_pools_id,
    userPoolWebClientId: config.aws_user_pools_web_client_id,
    identityPoolId: config.aws_cognito_identity_pool_id
  }
})

const PrivateRoute = ({children, path}: any) => {
  const auth = useAuth()
  return <Route
          path={path}
          render={
            ({location}) =>
                    auth.user && auth.user.trim() !== '' ? children :
                            <Redirect to={{pathname: '/login', state: {from: location}}}/>
          }/>
}

const NoMatch = () => {
  const location = useLocation()
  return <div>
    <h3>No Match for route {location.pathname}.</h3>
  </div>
}

const App = () => {
  return <AuthProvider>
    <ConferenceProvider
            conferencesApi={new Conferences()}>
      <Router>
        <Switch>
          <Route exact path="/">
            <Redirect to="/profile"/>
          </Route>
          <Route path="/login">
            <LoginScreen/>
          </Route>
          <Route exact path="/confirm">
            <AttendeeProvider attendeesApi={new Attendees()}>
              <ConfirmationScreen/>
            </AttendeeProvider>
          </Route>
          <PrivateRoute path="/profile">
              <BookingProvider bookingApi={new BookingImpl()}>
                <ProfileProvider profileApi={new Profile()}>
                  <ProfileScreen/>
                </ProfileProvider>
              </BookingProvider>
          </PrivateRoute>
          <Route path="*">
            <NoMatch/>
          </Route>
        </Switch>
      </Router>
    </ConferenceProvider>
  </AuthProvider>
}

export default App
