const API_ENDPOINT: string = 'https://api.socrates-conference.de'
export const config = {
  conference: 'socrates23',
  logoPath: 'https://socrates-sponsors-data-logo-upload.s3.amazonaws.com',
  apiEndpoints: {
    newsletters: API_ENDPOINT,
    sponsors: API_ENDPOINT,
    slots: API_ENDPOINT,
    subscribers: API_ENDPOINT,
    templates: API_ENDPOINT,
    conferences: API_ENDPOINT,
    applicants: API_ENDPOINT,
    attendees: API_ENDPOINT,
    booking: API_ENDPOINT,
    profile: API_ENDPOINT,
    rooms: API_ENDPOINT
  },
  minimumSponsoringAttendees: 190
}